package br.ucsal.bes20191.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.ucsal.bes20191.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;

import static org.mockito.Mockito.*;

import java.time.LocalDate;

public class AlunoBOUnitarioTest {

	AlunoDAO alunoMock;
	DateHelper dateMock;
	AlunoBO aluno;

	@Before
	public void iniciar() {
		dateMock = mock(DateHelper.class);
		alunoMock = mock(AlunoDAO.class);
		aluno = new AlunoBO(alunoMock, dateMock);
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter� 16
	 * anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		Integer matricula = 200;
		Integer idade = 16;

		Mockito.doReturn(LocalDate.now().getYear()).when(dateMock).obterAnoAtual();

		Aluno novoAluno = AlunoBuilder.umAluno().comMatricula(matricula).comAnoNascimento(2003).build();

		doReturn(novoAluno).when(alunoMock).encontrarPorMatricula(matricula);

		Integer idadeAtual = aluno.calcularIdade(matricula);

		Assert.assertEquals(idade, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {

		Aluno veriAluno = AlunoBuilder.umAluno().build();
		aluno.atualizar(veriAluno);
		verify(alunoMock).salvar(veriAluno);

	}

}
