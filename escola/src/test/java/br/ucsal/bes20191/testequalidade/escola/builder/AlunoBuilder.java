package br.ucsal.bes20191.testequalidade.escola.builder;

import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {

	private static final Integer MATRICULA_DEFAULT = 1;
	private static final String NOME_DEFAULT = "AlunoBuild";
	private static final SituacaoAluno SITUACAO_DEFAULT = SituacaoAluno.ATIVO;
	private static final Integer ANO_NASCIMENTO_DEFAULT = 1990;

	private Integer matricula = MATRICULA_DEFAULT;
	private String nome = NOME_DEFAULT;
	private SituacaoAluno situacao = SITUACAO_DEFAULT;
	private Integer anoNascimento = ANO_NASCIMENTO_DEFAULT;

	public AlunoBuilder() {
	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public AlunoBuilder comAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder comMatriculaAtiva() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}

	public AlunoBuilder comMatriculaCancelada() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}

	public Aluno build() {
		Aluno aluno = new Aluno();

		aluno.setNome(nome);
		aluno.setMatricula(matricula);
		aluno.setSituacao(situacao);
		aluno.setAnoNascimento(anoNascimento);

		return aluno;
	}
}
