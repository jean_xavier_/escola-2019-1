package br.ucsal.bes20191.testequalidade.escola.tui;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class TuiHelperUnitarioTest {

	/**
	 * Verificar a obtenção do nome completo. Caso de teste: primeiro nome "Claudio"
	 * e sobrenome "Neiva" resulta no nome "Claudio Neiva".
	 */
	@Test
	public void testarObterNomeCompleto() {
		
		TuiUtil tuiUtil = new TuiUtil();
		
		Scanner scanMock = mock(Scanner.class);
		
		tuiUtil.scanner = scanMock;
		
		String firstName = "Claudio";
		String lastName = "Neiva";
		
		String expectedName = "Claudio Neiva";
		
		doReturn(firstName).doReturn(lastName).when(scanMock).nextLine();
		
		String fullName = tuiUtil.obterNomeCompleto();

		Assert.assertEquals(expectedName, fullName);
	}

	/**
	 * Verificar a obtenção exibição de mensagem. Caso de teste: mensagem "Tem que
	 * estudar." resulta em "Bom dia! Tem que estudar.".
	 */
	@Test
	public void testarExibirMensagem() {
		TuiUtil tuiUtil = new TuiUtil();
		
		PrintStream printMock = mock(PrintStream.class);
		System.setOut(printMock);
		
		String mensagem = "Tem que estudar.";
		
		tuiUtil.exibirMensagem(mensagem);
		
		Mockito.verify(printMock).print("Bom dia! ");
		Mockito.verify(printMock).println(mensagem);
		
	}

}
